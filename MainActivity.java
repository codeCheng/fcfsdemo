package com.example.mevur.fcfs;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private String[] thread = new String[12];
    private int front = 0;
    private int rear = 0;
    private ProgressBar running;
    private MyHandler progressHandler;
    private RunThread runThread;
    private TextView threadPool;
    private LinearLayout bg;
    private TextView run;
    private TextView runingInfo;
    private boolean hasThreadRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //初始化ui控件
        init();
    }
    private void init() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //透明导航栏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        running = (ProgressBar) findViewById(R.id.prg_run);
        progressHandler = new MyHandler();
        runThread = new RunThread();
        threadPool = (TextView) findViewById(R.id.thread_pool);
        bg = (LinearLayout) findViewById(R.id.bg_run);
        run = (TextView) findViewById(R.id.txt_run_name);
        runingInfo = (TextView) findViewById(R.id.txt_run_info);
        bg.setBackgroundColor(Color.parseColor("#ffffff"));
        running.setProgress(0);
        run.setText("");
        runingInfo.setText("");
        threadPool.setText("");
        for (int i = 0; i < 12; i++) {
            thread[i] = "";
        }
    }

    private void start() {
        if (front != rear) {
            hasThreadRunning = true;
            switch (thread[rear]) {
                case "红": {
                    bg.setBackgroundColor(Color.parseColor("#FF0000"));
                    run.setText("红");
                    runingInfo.setText("Red thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                case "黑": {
                    bg.setBackgroundColor(Color.parseColor("#000000"));
                    run.setText("黑");
                    runingInfo.setText("Black thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                case "绿": {
                    bg.setBackgroundColor(Color.parseColor("#008000"));
                    run.setText("绿");
                    runingInfo.setText("Green thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                case "紫": {
                    bg.setBackgroundColor(Color.parseColor("#9370DB"));
                    run.setText("紫");
                    runingInfo.setText("Purple thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                case "蓝": {
                    bg.setBackgroundColor(Color.parseColor("#8A2BE2"));
                    run.setText("蓝");
                    runingInfo.setText("Blue thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }case "粉": {
                    bg.setBackgroundColor(Color.parseColor("#C71585"));
                    run.setText("粉");
                    runingInfo.setText("Pink thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                case "灰": {
                    bg.setBackgroundColor(Color.parseColor("#2f4f4f"));
                    run.setText("灰");
                    runingInfo.setText("Gray thread is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                case "青": {
                    bg.setBackgroundColor(Color.parseColor("#1E90FF"));
                    run.setText("青");
                    runingInfo.setText("Dodger is running...");
                    runThread = new RunThread();
                    runThread.start();
                    rear++;
                    refreshUI();
                    break;
                }
                default: {
                    break;
                }
            }
        }
        else {
            hasThreadRunning = false;
        }
    }

    private String refreshThreadPool(String threadName) {
        //将新线程加入线程池
        if ((front + 1) % 12 == rear ) {
            return "线程池已满,添加线程失败";
        }
        else {
            thread[front] = threadName;
            front = (front + 1) % 12;
            int fina;
            if (front > rear) {
                fina = front;
            }
            else {
                fina = front + 12;
            }
            StringBuffer sb = new StringBuffer();
            boolean isadd = false;
            sb.append("");
            for (int i = fina - 1; i >= rear; i--) {
                isadd = true;
                sb.append(thread[i % 12]).append(" ");
            }
            if (isadd) {
                sb.deleteCharAt(sb.length() - 1);
            }
            threadPool.setText(sb.toString());
            return "添加" + threadName + "到线程池成功";
        }
    }
    public void refreshUI() {
        StringBuffer sb = new StringBuffer();
        boolean isadd = false;
        int fina;
        if (front > rear) {
            fina = front;
        }
        else if (front == rear){
            fina = rear;
        }else {
            fina = front + 12;
        }
        sb.append("");
        for (int i = fina - 1; i >= rear; i--) {
            isadd = true;
            sb.append(thread[i % 12]).append(" ");
        }
        if (isadd) {
            sb.deleteCharAt(sb.length() - 1);
        }
        threadPool.setText(sb.toString());
    }


    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle data = msg.getData();
            int progress = data.getInt("progress");
            Log.i("progress", "" + progress);
            if (progress > 100) {
                //// TODO: 11/30/2016
                //执行下一个线程
                start();
            }
            else {
                running.setProgress(progress);
            }
        }
    }

    private class RunThread extends Thread {
        @Override
        public void run() {
            super.run();
            for (int i = 0; i <= 101; i++) {
                Message msg = new Message();
                Bundle data = new Bundle();
                data.putInt("progress", i);
                msg.setData(data);
                progressHandler.sendMessage(msg);
                try {
                    sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }

    public void btn_red_clicked(View view) {
        String response = refreshThreadPool("红");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_black_clicked(View view) {
        String response = refreshThreadPool("黑");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_blue_clicked(View view) {
        String response = refreshThreadPool("蓝");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_green_clicked(View view) {
        String response = refreshThreadPool("绿");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_purple_clicked(View view) {
        String response = refreshThreadPool("紫");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_gray_clicked(View view) {
        String response = refreshThreadPool("灰");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_pink_clicked(View view) {
        String response = refreshThreadPool("粉");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }
    public void btn_sky_clicked(View view) {
        String response = refreshThreadPool("青");
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        if (!hasThreadRunning) {
            start();
        }
    }


}
